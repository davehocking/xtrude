# Written by Dave Hocking, Systems Engineer, Nutanix : @daveyb0y / dhocking@nutanix.com
# Version 1.0

# PLEASE READ THE README FILE FOR INSTALLATION/UPGRADE/USAGE INSTRUCTIONS

# Requires PowerShell Core (Windows, Mac & Linux), due to the inclusion of the -SkipCertificateCheck switch in the in invoke-webrequest cmdlet
# Also requires that the target Prism Element is running AHV - ESXi and Hyper-V haven't been tested

# PrismCentral APIs haven't (yet) been written into this module

# This Module started life as a "build-out" script for Hosted POCs at Nutanix, however, some of the functions are useful for other tasks
# Bear with me while I tidy this up and make it more suitable for generic use, it's not my day-job and I'm doing what I can, when I can!

function Import-XtrdWinCreds()
{
    <#
        .SYNOPSIS
            version 0.1 - Written by Dave Hocking, Nutanix SE, December 2018.
            Created to make Automating Xtrude tasks via Windows Task Scheduler possible
            Written for PowerShell Core on Windows

           Uses the Export/Import-CliXML function, which securely stores credentials
           See the ReadMe for more info on creating these credentials.


        .DESCRIPTION
            Requires:
            - A credentials file (.xml) that was created by the user that wants to run the tasks
            - The use of "run whether user is logged on or not" in task scheduler    
            
            Optional -prism can be specified to test credentials are valid

        .EXAMPLE
        As long as the variable $creds is used, Xtrude will be able to login as the user in the secured xml file
        ..in this example, Invoke-XtrdPrismAPI is being called directly

        $creds = Import-XtrdWinCreds -xmlFile creds.xml
        Invoke-XtrdPrismAPI -prism 10.21.86.37 -body "{}" -method get -apiPath "networks" -apiVer "v2"
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$xmlFile,
    [Parameter(Mandatory=$false)]
    [string]$prism
    )

    # Test for the path being correct, does a file exist
    if ((Test-Path -path $xmlFile) -eq $false)
    {  
        Write-Warning "Cannot continue - specify valid path to Creds"
        break
    }

    # Wipe out any existing cached creds
    $creds = $null

    # Import the XML file
    $creds = Import-Clixml -Path $xmlFile
    # Handle incorrect import, corrupt file etc
    if (!$creds)
    {
        Write-Warning "Cannot continue - something prevented the creds from being imported"
        break
    }

    # If prism details were entered, test if the cluster type can be returned OK.
    if ($PSBoundParameters.ContainsKey('prism'))
    {
        $test = Get-XtrdClusterType -prism $prism
        if (!$test)
        {
            Write-Warning "Credentials specified are invalid, or another issue prevented Get-XtrdClusterType from working"
            break
        }
        else
        {
            Write-Host "Credentials successfully imported and tested OK."
        }
    }
    else
    {
        Write-Host "Credentials file sucessfully imported, credentials not tested yet."    
    }



}

function Invoke-XtrdPrismAPI()
{
    <#
    .SYNOPSIS
        version 1.1 - Written by Dave Hocking, Nutanix SE, April 2018.
        Created to make PowerShell API Calls into the Nutanix RestAPI.

        Currently this requires PowerShell Core (due to the -skipcertificatecheck switch needed).
        A workaround could be made for PowerShell, this just hasn't been prioritised yet!


    .DESCRIPTION
        Requires:
        - a Prism Element IP or FQDN
        - valid credentials it will collect in each session
        - some body (as JSON)
        - an API version
        - URL/URI for the call
        - method
        - some variables/switches are optional

    .EXAMPLE
    Will grab the networks from a Prism Element server, on the full URL "https://prismIP/PrismGateway/services/rest/v2.0/networks"
    NB that the -apiPath string is only the unique part of the URL, the previous portions are fixed by the Prism IP and the API ver
    
    $response = Invoke-XtrdPrismAPI -prism 10.21.86.37 -body "{}" -method get -apiPath "networks" -apiVer "v2"
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$body,
    [Parameter(Mandatory=$true)]
    [ValidateSet('get','post','put','delete')]
    [string]$method,
    [Parameter(Mandatory=$false)]
    [string]$contentType="application/json",
    [ValidateSet('v0.8','v1','v2','v3')]
    [Parameter(Mandatory=$false)]
    [string]$apiVer="v2",
    [Parameter(Mandatory=$true)]
    [string]$apiPath
    )

    # Check for the presence of creds, optionally capture them, saves you having to keep re-entering them
    if (!$creds) {$global:creds = Get-Credential -Message "Enter your Prism Element Credentials"}

    # Construct the auth header
    $header = @{"Authorization" = "Basic "+[System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($creds.username+":"+$creds.GetNetworkCredential().password ))}
    
    # Start building the API URI
    $baseURL = "https://$($prism):9440"

    # Check for the appropriate API version and handle the URI's accordingly
    if ($apiVer -eq "v3")
    {
        $baseURL = $baseURL+"/api/nutanix/v3/"
    }

    if ($apiVer -eq "v2")
    {
        $baseURL = $baseURL+"/PrismGateway/services/rest/v2.0/"
    }

    if ($apiVer -eq "v1")
    {
        $baseURL = $baseURL+"/PrismGateway/services/rest/v1/"
    }

    if ($apiVer -eq "v0.8")
    {
        $baseURL = $baseURL+"/api/nutanix/v0.8/"
    }

    # Add the API Path (from Parameters) to the BaseURL
    $apiURL = $baseURL+$apiPath

    # Test for PowerShell Core (needed for the skipcertificatecheck)
    if ($PSVersionTable.PSEdition -eq "Core")
    {
        # Insert test here for correct auth, if not clean up the creds and try again
        # Create a loop with three attempts before breaking out, cleaning up the creds on the way out!

        Invoke-WebRequest -Uri $apiURL -Method $method -Headers $header -ContentType $contentType -UseBasicParsing -SkipCertificateCheck -Body $body
    }
    else
    {
        Write-Host "Not supported for Desktop edition of PowerShell as yet..."    
    }
}

function Get-XtrdClusterUnresolvedCriticalAlerts()
{
    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism
    )

    # Grab the clusters (only one will be returned from Prism Element)
    $apiVer = "v2"
    $apiPath = "alerts/?resolved=false$&severity=critical"
    $method = "get"
    $body =
@"
    {
    }
"@

    $unresolvedCriticalAlertsResponse = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    # Find any objects that match the Critical and Unresolved status for alerts
    $unresolvedCriticalAlerts = ($unresolvedCriticalAlertsResponse | ConvertFrom-Json).entities
    if(@($unresolvedCriticalAlerts).count -gt 0){Write-Warning "We found one or more UNRESOLVED CRITICAL alerts on the cluster";$unresolvedCriticalAlerts.alert_title | Get-unique;break}
    if(@($unresolvedCriticalAlerts).count -lt 1){Write-Host "We found no unresolved, critical alerts on the cluster";break}
}

function Get-XtrdClusterType()
{
    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism
    )

    # Grab the clusters (only one will be returned from Prism Element)
    $apiVer = "v3"
    $apiPath = "clusters/list"
    $method = "post"
    $body =
@"
    {
    }
"@

    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    # Find any objects that match the defined name
    $nodeTypes = ($response | ConvertFrom-Json).entities.status.resources.nodes.hypervisor_server_list.type | Get-Unique
    if(@($nodeTypes).length -gt 1){Write-Warning "We found more than one returned Hypervisor type in the cluster";break}
    if(@($nodeTypes).length -lt 1){Write-Warning "We didn't find any Hypervisor types in the cluster";break}
    $nodeTypes
}

function Test-XtrdTaskStatus()
{
    <#  .SYNOPSIS
        version 0.1 - Written by Dave Hocking 2018.

        This function will check (and report on) or wait for a specifed Nutanix task state.
        If the operation is to "wait" for the state specified, you can set a timeout value
        If the timeout value is exceeded and the desired state hasn't changed, an error is thrown

        Requires a UUID the task in Prism - returned when you submit a job via the API

        .DESCRIPTION
        Here are the possible states you can check for:

        NAME	             DESCRIPTION
        aborted              The task has been forcefully aborted
        cancelled            The task was requested to be cancelled
        failed               The task failed to complete
        queued               The task hasn't yet started, it's in a queue
        running              The task is running
        paused               The task has been paused
        skipped              The task has been skipped
        succeeded            The task has completed
        scheduled            The task runs on a schedule

        .EXAMPLE
        If you want to script a task that should wait 5 mins for the task at $taskUUID to switch to "Success", testing every 2 seconds:
        Test-XtrdTaskStatus $taskUUID -wait -for success -mins 5 -intervalSecs 2

        .EXAMPLE
        If you want to script a task that should wait 2 mins for the task at $taskUUID to switch to "Success":
        Test-XtrdTaskStatus -prism 10.21.22.37 $taskUUID -wait -for success -mins 2 -quiet
         - The quiet switch will prevent periodic reporting of the task status

        .EXAMPLE
        If you wanted to return $true or $false for the status or "failed":
        Test-XtrdTaskStatus -prism 10.21.22.37 $taskUUID -check -for failed

        .EXAMPLE
        If you wanted to simply report the task state given it's $taskUUID:
        Test-XtrdTaskStatus -prism 10.21.22.37 $taskUUID -report
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]

    # Define the Parameters to pass the to function
    Param(
    # Require a task uuid
    [Parameter(Mandatory=$True)]
    $prism,
    [Parameter(Mandatory=$True)]
    $taskUUID,
    [Parameter(Mandatory=$true,ParameterSetName="Wait")]
    [switch]$wait,
    [Parameter(Mandatory=$false,ParameterSetName="Wait")]
    [Int]$intervalSecs,
    [Parameter(Mandatory=$true,ParameterSetName="Check")]
    [switch]$check,
    [Parameter(Mandatory=$true,ParameterSetName="Report")]
    [switch]$report,
    [Parameter(Mandatory=$true,ParameterSetName="Check")]
    [Parameter(Mandatory=$true,ParameterSetName="Wait")]
    [ValidateSet("aborted","cancelled","failed","queued","running","paused","skipped","succeeded","scheduled")]
    [string]$for,
    # Validate that any -mins specified are with the range below
    [Parameter(ParameterSetName="Wait")]
    [ValidateRange(1,300)]
    [Int]$mins,
    # Optional -quiet switch to prevent %-status reporting
    [Parameter(ParameterSetName="Wait")]
    [switch]$quiet
    )

    # Specify the body to get the task details
    $apiVer = "v3"
    $apiPath = "tasks/$($taskUUID)"
    $method = "get"
    $body =
@"
{
}
"@

    # Re-write variable for clarity
    $desiredStatus = $for

    # Set up the "wait" operation
    if ($wait)
    {
        if(!$PSBoundParameters.ContainsKey("quiet"))
        {
            Write-Host "Waiting for task status of " -NoNewline
            Write-Host "$desiredStatus" -ForegroundColor Yellow -NoNewline
            Write-Host "..."
        }

        # If $interval hasn't been populated, use a default of 10
        if (!$intervalSecs)
        {
            $intervalSecs = 10
        }

        # If $mins hasn't been populated, use a default of 15
        if (!$mins)
        {
            $mins = 15
        }

        # Determine the correct number of iterations to run through
        $iterations = ($mins*60)/$intervalSecs

        # Create loop to run for the specified length of time
        for ($i=1; $i -le $iterations; $i++)
        {
            start-sleep $intervalSecs
            $task = Invoke-XtrdPrismAPI -prism $prism -method $method -body $body -apiVer $apiVer -apiPath $apiPath
            $taskUUIDStatus = ($task.Content|convertfrom-json).status
            $pctComplete = ($task.Content|convertfrom-json).percentage_complete

            # Check for -quiet switch and alter output as required
            if (!$PSBoundParameters.ContainsKey("quiet"))
            {
                write-host "`rTask Status is: " -NoNewline
                Write-Host "$($taskUUIDStatus)" -ForegroundColor Yellow -NoNewline
                write-host " / " -NoNewline
                Write-Host "$($pctComplete)%" -ForegroundColor Yellow -NoNewline
            }

            # Check if the actual task status matches any of the desiredStatus combinations
            if ($desiredStatus -match $taskUUIDStatus)
            {
                $result = "complete"
                break
            }

            # Check if the task status matches any of "aborted","cancelled","failed"
            if ($taskUUIDStatus -match "aborted" -or $taskUUIDStatus -match "cancelled" -or $taskUUIDStatus -match "failed")
            {
                Write-Warning "Stopping due to fail/abort/cancel"
                break
            }
        }

        if ($result)
        {
            # Optionally output
            if(!$PSBoundParameters.ContainsKey("quiet"))
            {
                ""
                "Desired status of `"" + $desiredStatus + "`" was reached."
            }
        }

        # If status doesn't change, display error and stop script
        else
        {
            write-warning "Failed to reach desired status within the time allocated."
            $task = Invoke-XtrdPrismAPI -prism $prism -method $method -body $body -apiVer $apiVer -apiPath $apiPath
            $taskStatus = ($task.Content|convertfrom-json).status
            $pctComplete = ($task.Content|convertfrom-json).percentage_complete
            Write-Host "The task status is `"" -NoNewline
            Write-Host "$($taskStatus)" -ForegroundColor Yellow -NoNewline
            Write-Host "`" and it's " -NoNewline
            Write-Host "$($pctComplete)" -NoNewline -ForegroundColor DarkYellow
            Write-Host "% complete."
            break
        }
    }

    # Set up the "check" operation
    if ($check)
    {
        $task = Invoke-XtrdPrismAPI -prism $prism -method $method -body $body -apiVer $apiVer -apiPath $apiPath
        $taskStatus = ($task.Content|convertfrom-json).status
        if ($taskStatus -eq $desiredStatus)
        {
        return $true
        }
        else
        {
        return $false
        }
    }

    # Set up the "report" operation
    if ($report)
    {
        $task = Invoke-XtrdPrismAPI -prism $prism -method $method -body $body -apiVer $apiVer -apiPath $apiPath
        $taskStatus = ($task.Content|convertfrom-json).status
        $pctComplete = ($task.Content|convertfrom-json).percentage_complete
        $errorDetail = ($task.Content|convertfrom-json).error_detail
        Write-Host "The task status is `"" -NoNewline
        Write-Host "$($taskStatus)" -ForegroundColor Yellow -NoNewline
        Write-Host "`" and it's " -NoNewline
        Write-Host "$($pctComplete)" -NoNewline -ForegroundColor DarkYellow
        Write-Host "% complete."
        Write-Host "$($errorDetail)" -ForegroundColor Red
    }
}

function Get-XtrdStorageContainer()
{
    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$storageContainerName
    )

    # Grab the containers
    $apiVer = "v2"
    $apiPath = "storage_containers"
    $method = "get"
    $body =
@"
    {
        "metadata": {
        "search_string": ""
        }
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    # return any objects from the call
    ($response | ConvertFrom-Json).entities | Where-Object {$_.name -eq $storageContainerName}
}

function New-XtrdStorageContainer()
{
    <#
    .SYNOPSIS
        version 0.1 - Written by Dave Hocking, Nutanix SE, Nov 2018.
        Creates Storage Containers against Prism Element using supplied details

    .DESCRIPTION
        Creates a new storage container with a given name, optional size and with an optional delay for compression
        There is no option for disabling compression in this version of the function, it wasn't an obvious API call
        and I didn't have time to debug it - besides, compression is great, right??

        This could be expanded with switches for ECx and DeDupe, plus RF values and Reserved Space/Whitelists.

    .EXAMPLE
    Will create a 3TiB "MyStorage" container, with inline compression enabled
    $response = New-XtrdStorageContainer -prism 10.21.86.37 -ContainerName "MyStorage" -sizeGiB 3000 -compressDelayMins 0

    .EXAMPLE
    Will create a "MyStorage2" container, with post-process compression enabled (defaults to 1 Hour)
    The advertised size will set by the free space in the pool (thin provisioned)
    $response = New-XtrdStorageContainer -prism 10.21.86.37 -ContainerName "MyStorage2"
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$containerName,
    [Parameter(Mandatory=$false)]
    [int32]$sizeGiB,
    [Parameter(Mandatory=$false)]
    [ValidateRange(0,9999999)]
    [int32]$compressDelayMins=60
    )

    # Convert GiB to Bytes for the JSON body
    $sizeB = $sizeGiB*1073741824

    # Convert Compression Delay from Mins to Secs
    $compressDelaySecs = $compressDelayMins * 60

    # Handle any missing values for size
    if ($PSBoundParameters.ContainsKey('sizeGiB'))
    {
        $sizeStatement = "`"advertised_capacity`": $($sizeB),"
    }
    else 
    {
        $sizeStatement = ""
    }

    $apiVer = "v2"
    $apiPath = "storage_containers"
    $method = "post"
    $body =
@"
{
    $($sizeStatement)
    "compression_delay_in_secs": $($compressdelaySecs),
    "compression_enabled": true,
    "name": "$($containerName)"
}
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer

    # return any objects from the call, True for success
    ($response | ConvertFrom-Json).value
}

function Set-XtrdStorageContainer()
{
    <#
    .SYNOPSIS
        version 0.1 - Written by Dave Hocking, Nutanix SE, Nov 2018.
        Changes Storage Container settings, against Prism Element using supplied details

    .DESCRIPTION
        Adjusts a storage container with a given name, initially this will just support enable/disable compression
        This should be expanded with switches for ECx and DeDupe, plus RF values and Reserved Space/Whitelists.

    .EXAMPLE
    Will enable compression on the "MyStorage" container, with the default delay of 0s (inline)
    $response = Set-XtrdStorageContainer -prism 10.21.86.37 -ContainerName "MyStorage" -compress true
    #>

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$containerName,
    [ValidateSet('true','false')]
    [string]$compress
    )

    # grab the container UUID
    $container = Get-XtrdStorageContainer -prism $prism -storageContainerName $containerName
    $containerUUID = $container.storage_container_uuid
    $containerName = $container.name


    $apiVer = "v1"
    $apiPath = "containers"
    $method = "put"
    $body =
@"
{
    "compressionDelayInSecs": "0",
    "compressionEnabled": $($compress),
    "containerUuid": "$($containerUUID)",
    "name": "$($containerName)",
    "enableSoftwareEncryption": false
}
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer

    # return any objects from the call, True for success
    ($response | ConvertFrom-Json).value
}

function Get-XtrdNetwork()
{
    # Returns an object, including UUID for a given network name or VLANid

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$false)]
    [string]$networkName,
    [Parameter(Mandatory=$false)]
    [string]$networkVLANid
    )

    # Grab the UUID for the network
    $apiVer = "v2"
    $apiPath = "networks"
    $method = "get"
    $body =
@"
    {
        "metadata": {
        "search_string": ""
        }
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
   
    if ($PSBoundParameters.ContainsKey('networkName')) 
    {
        ($response | ConvertFrom-Json).entities | Where-Object {$_.name -eq $networkName}
    }
   
    if ($PSBoundParameters.ContainsKey('networkVLANid')) 
    {
        ($response | ConvertFrom-Json).entities | Where-Object {$_.vlan_id -eq $networkVLANid}
    }
}

function New-XtrdNetwork()
{
    # Creates a network with given name / VLANid and optionally with/without IPAM

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [Parameter(Mandatory=$true,ParameterSetName="noIPAM")]
        [string]$prism,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [Parameter(Mandatory=$true,ParameterSetName="noIPAM")]
        [string]$name,

        [ValidateRange('0','4094')]
        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [Parameter(Mandatory=$true,ParameterSetName="noIPAM")]
        [int32]$vlanID,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetaddr,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetCIDRbits,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetGWaddr,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetIPstart,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetIPstop,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetDomain,

        [Parameter(Mandatory=$true,ParameterSetName="IPAM")]
        [string]$vmNetDNS
    )

    $apiVer = "v2"
    $apiPath = "networks"
    $method = "post"

    # Define the correct JSON Body for IPAM/NoIPAM mode
    $ipamBody = 
@"
    {
    "ip_config": {
    "default_gateway": "$($vmNetGWaddr)",
    "dhcp_options": {
        "domain_name": "$($vmNetDomain)",
        "domain_name_servers": "$($vmNetDNS)",
        "domain_search": "$($vmNetDomain)"
    },
    "network_address": "$($vmNetAddr)",
    "pool": [
        {
        "range": "$($vmNetIPstart) $($vmNetIPstop)"
        }
    ],
    "prefix_length": $($vmNetCIDRbits)
    },
    "name": "$($name)",
    "vlan_id": $($vlanID)
    }
"@

$noIPAMBody = 
@"
    {
    "name": "$($name)",
    "vlan_id": $($vlanID)
    }
"@

    # Check for IPAM/NoIPAM mode and use the respective JSON for the call
    if($PSBoundParameters.ContainsKey('vmNetAddr'))
    {
        $taskResponse = Invoke-XtrdPrismAPI -prism $prism -body $ipamBody -method $method -apiPath $apiPath -apiVer $apiVer
        # If successful, the uuid is returned
        ($taskResponse | ConvertFrom-Json).network_uuid
    }

    else
    {
        $taskResponse = Invoke-XtrdPrismAPI -prism $prism -body $noIPAMBody -method $method -apiPath $apiPath -apiVer $apiVer
        # If successful, the uuid is returned
        ($taskResponse | ConvertFrom-Json).network_uuid
    }
}

function Get-XtrdImage()
{
    # Returns an object for a given vDisk Image Name

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$imgName
    )

    # Grab the the vDisk template image
    $apiVer = "v2"
    $apiPath = "images"
    $method = "get"
    $body =
@"
    {
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    ($response | ConvertFrom-Json).entities | Where-Object {$_.name -match $imgName}
}

function Get-XtrdImageV3()
{
    # Returns an object for a given vDisk Image Name (v3 API)

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$imgName
    )

    # Grab the the vDisk template image
    $apiVer = "v3"
    $apiPath = "images/list"
    $method = "post"
    $body =
@"
    {
        "filter": "name==$($imgName)",
        "kind": "image"
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    #write-host "$($response.StatusDescription)" -ForegroundColor Blue -NoNewline
    ($response | ConvertFrom-Json).entities
}

function New-XtrdImage()
{
    # Creates a new Image for a given source vdisk address, image name, image type and destination container
    # v3 API was broken for this function at the time of writing [2018.05.01 - Community License]

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$srcVdiskAddr,
    [Parameter(Mandatory=$true)]
    [string]$destContainerName,
    [Parameter(Mandatory=$true)]
    [string]$imageName,
    [ValidateSet('ISO_IMAGE','DISK_IMAGE')]
    [Parameter(Mandatory=$true)]
    [string]$imageType
    )

    # Specify the body to create the image spec
    $apiVer = "v2"
    $apiPath = "images"
    $method = "post"
    $body =
@"
    {
        "annotation": "Created by xtrude",
        "image_import_spec": {
        "storage_container_name": "$($destContainerName)",
        "url": "nfs://127.0.0.1$($srcVdiskAddr)"
        },
        "image_type": "$($imageType)",
        "name": "$($imageName)"
    }
"@
    Write-Host "Submitting request to create image: " -ForegroundColor Yellow -NoNewline
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    if ($response.StatusCode -eq "201")
    {
        Write-Host "Task UUID " -NoNewline
        Write-Host " [ " -ForegroundColor Yellow -NoNewline
        Write-Host "$(($response.Content|ConvertFrom-Json).task_uuid)" -NoNewline -ForegroundColor Cyan
        Write-Host " ]" -ForegroundColor Yellow

    }
    else
    {
        ""
        Write-Warning "Task submission encountered an error - HTTP:$($response.StatusCode) / $($response.StatusDescription)"   
    }
    ($response.Content|ConvertFrom-Json).task_uuid
}

function Remove-XtrdImage()
{
    # Removes an Image for a given image UUID, using the v0.8 API

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$imageUUID
    )

    # Specify the body to create the image spec
    $apiVer = "v0.8"
    $apiPath = "images/$($imageUUID)"
    $method = "delete"
    $body =
@"
    {
    }
"@
    Write-Host "Submitting request to remove image: " -ForegroundColor Yellow -NoNewline
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    if ($response.StatusCode -eq "200")
    {
        Write-Host "Task UUID " -NoNewline
        Write-Host " [ " -ForegroundColor Yellow -NoNewline
        Write-Host "$(($response.Content|ConvertFrom-Json).taskuuid)" -NoNewline -ForegroundColor Cyan
        Write-Host " ]" -ForegroundColor Yellow

    }
    else
    {
        ""
        Write-Warning "Task submission encountered an error - HTTP:$($response.StatusCode) / $($response.StatusDescription)"   
    }
    ($response.Content|ConvertFrom-Json).taskuuid
}

function Get-XtrdVdisks()
{
    # Returns an object listing all vDisks on a cluster

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism
    )

    # Grab the the vDisk template image
    $apiVer = "v2"
    $apiPath = "virtual_disks"
    $method = "get"
    $body =
@"
    {
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    ($response | ConvertFrom-Json).entities
}

function Get-XtrdVdisk()
{
    # Returns an object listing a vdisk for a given VM, adapter type and device index

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$vmName,
    [ValidateSet('SCSI','IDE','PCI')]
    [Parameter(Mandatory=$true)]
    [string]$adapterType,
    [ValidateRange('0','15')]
    [Parameter(Mandatory=$true)]
    [string]$deviceIndex
    )

    $vm = Get-XtrdVM -prism $prism -vmName $vmName
    $vm.vm_disk_info | Where-Object {$_.disk_address.disk_label -match "$($adapterType).$($deviceIndex)"}
}

function New-XtrdVdisk()
{
    # Creates a new vdisk, optionally from a source image

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true,ParameterSetName="clone")]
    [Parameter(Mandatory=$true,ParameterSetName="create")]
    [string]$prism,

    [Parameter(Mandatory=$true,ParameterSetName="clone")]
    [Parameter(Mandatory=$true,ParameterSetName="create")]
    [string]$vmUUID,

    [Parameter(Mandatory=$false,ParameterSetName="clone")]
    [switch]$clone,

    [Parameter(Mandatory=$false,ParameterSetName="create")]
    [switch]$create,

    [ValidateSet('true','false')]
    [Parameter(Mandatory=$false,ParameterSetName="clone")]
    [Parameter(Mandatory=$false,ParameterSetName="create")]
    [string]$isCDROM="false",

    [ValidateSet('SCSI','IDE','PCI','SATA','SPAPR')]
    [Parameter(Mandatory=$false,ParameterSetName="clone")]
    [Parameter(Mandatory=$false,ParameterSetName="create")]
    [string]$adapterType="SCSI",

    [ValidateRange(0,15)]
    [Parameter(Mandatory=$true,ParameterSetName="clone")]
    [Parameter(Mandatory=$true,ParameterSetName="create")]
    [int32]$diskAddress,

    [Parameter(Mandatory=$true,ParameterSetName="create")]
    [Int64]$vdiskSizeGiB,

    [Parameter(Mandatory=$true,ParameterSetName="create")]
    [string]$storageContainer,

    [Parameter(Mandatory=$true,ParameterSetName="clone")]
    [string]$vdiskImageUUID
    )

    # Convert the vDisk Size to Bytes, from GiB, if specified
    if($PSBoundParameters.ContainsKey('vdiskSizeGib'))
    {
        $vdiskSizeB = $vdiskSizeGiB*1024*1024*1024
    }

    # Get the Storage Container UUID if a container name was specified
    if($PSBoundParameters.ContainsKey('storageContainer'))
    {
        $storageContainerUUID = ((Get-XtrdStorageContainer -prism $prism -storageContainerName $storageContainer) | Where-Object {$_.name -eq $storageContainer}).storage_container_uuid
    }

    # Grab the the vDisk template image
    $apiVer = "v2"
    $apiPath = "vms/$vmUUID/disks/attach"
    $method = "post"
    $createBody =
@"
{
    "uuid": "$($vmUUID)",
    "vm_disks": [
      {
        "disk_address": {
          "device_bus": "$($adapterType)",
          "device_index": $($diskAddress)
        },
        "is_cdrom": $($isCDROM),
        "is_scsi_pass_through": true,
        "is_thin_provisioned": true,
        "vm_disk_create": {
          "size": $($vdiskSizeB),
          "storage_container_uuid": "$($storageContainerUUID)"
        }
      }
    ]
  }
"@
    $cloneBody =
@"
{
    "uuid": "$($vmUUID)",
    "vm_disks": [
      {
        "disk_address": {
          "device_bus": "$($adapterType)",
          "device_index": $($diskAddress)
        },
        "is_cdrom": $($isCDROM),
        "is_scsi_pass_through": true,
        "is_thin_provisioned": true,
        "vm_disk_clone": {
          "disk_address": {
            "vmdisk_uuid": "$($vdiskImageUUID)"
          }
        }
      }
    ]
  }
"@
    
    # Handle the create/clone operation
    if($PSBoundParameters.ContainsKey('clone')) {$body = $cloneBody}
    if($PSBoundParameters.ContainsKey('create')) {$body = $createBody}
    
    # Submit the API Call

    Write-Host "Submitting request to attach vDisk: " -ForegroundColor Yellow -NoNewline
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    if ($response.StatusCode -eq "201")
    {
        Write-Host "Task UUID " -NoNewline
        Write-Host " [ " -ForegroundColor Yellow -NoNewline
        Write-Host "$(($response.Content|ConvertFrom-Json).task_uuid)" -NoNewline -ForegroundColor Cyan
        Write-Host " ]" -ForegroundColor Yellow
    }
    else
    {
        ""
        Write-Warning "Task submission encountered an error - HTTP:$($response.StatusCode) / $($response.StatusDescription)"   
    }
    ($response | ConvertFrom-Json).task_uuid
}

function Remove-XtrdVDisk()
{
    # Removes (detaches) a given vDisk for a VM UUID, returning a task object
    # e.g. use "$task = Remove-XtrdVDisk -prism 10.21.22.37 -vmUUID 8e133be9-d81e-4b85-874e-311bace488f9 -vDiskUUID " to create an object to hold the VM details
    

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$vmUUID,
    [Parameter(Mandatory=$true)]
    [string]$vDiskUUID
    )

    # Grab the UUID for the VM
    $apiVer = "v2"
    $apiPath = "/vms/$($vmUUID)/disks/detach"
    $method = "post"
    $body =
@"
{
    "vm_disks": [
        {"disk_address":
            {
                "vmdisk_uuid": "$($vDiskUUID)"
            }
        }]
}
"@

    Write-Host "Submitting request to detach vDisk: " -ForegroundColor Yellow -NoNewline
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    if ($response.StatusCode -eq "201")
    {
        Write-Host "Task UUID " -NoNewline
        Write-Host " [ " -ForegroundColor Yellow -NoNewline
        Write-Host "$(($response.Content|ConvertFrom-Json).task_uuid)" -NoNewline -ForegroundColor Cyan
        Write-Host " ]" -ForegroundColor Yellow

    }
    else
    {
        ""
        Write-Warning "Task submission encountered an error - HTTP:$($response.StatusCode) / $($response.StatusDescription)"   
    }

    # Return the task details
    ($response | ConvertFrom-Json).task_uuid
}

function Move-XtrdVdisk()
{
    # Moves a VM's disk/cdrom image between AHV Containers
    # e.g. Move-XtrdVdisk -prism 10.0.1.1 -vmName VM1 -diskType DISK -diskID 0 -adapterType SCSI -destContainerName VM-Data
    
    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$vmName,
    [ValidateSet('CDROM','DISK')]
    [Parameter(Mandatory=$false)]
    [string]$diskType="DISK",
    [Parameter(Mandatory=$true)]
    [ValidateRange(0,15)]
    [string]$diskID,
    [Parameter(Mandatory=$false)]
    [ValidateSet('SCSI','IDE','PCI')]
    [string]$adapterType="SCSI",
    [Parameter(Mandatory=$true)]
    [string]$destContainerName
    )
    
    Show-XtrdBanner

    Write-Host "Beginning Pre-Flight Checks..." -ForegroundColor Green
    # Test if the cluster is running AHV
    Write-Host "Testing for AHV" -NoNewline -ForegroundColor Yellow
    $ntnxClustertype = Get-XtrdClusterType -prism $prism
    if ($ntnxClustertype -ne "AHV")
    {
        ""
        Write-Warning "The target cluster isn't using AHV, other hypervisors haven't been coded for as yet"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }

    # Gets a VM object
    $vm = Get-XtrdVM -prism $prism -vmName $vmName

    # Checks for one VM being returned
    Write-Host "Testing for VM presence" -NoNewline -ForegroundColor Yellow
    if (@($vm).count -ne 1)
    {
        ""
        Write-Warning "Source VM name doesn't exist, or more than one was found - be more specific"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }

    # Checks for the VM being powered off
    Write-Host "Testing for VM Powered off" -NoNewline -ForegroundColor Yellow
    if ($vm.power_state -ne "off")
    {
        ""
        Write-Warning "Source VM not powered Off, no Live Migration possible at present, shutdown the VM"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }
    # Collect and display the returned vDisks
    $vDisks = $vm.vm_disk_info
    #write-host "I found these vDisks:"
    #$vDisks.device_properties.disk_address

    # Filters to list the disks that match the adapter type and disk id
    $vDisk = $vDisks | Where-Object {$_.disk_address.disk_label -match "$($AdapterType).$($diskID)"}

    # Checks for one vDisk being returned
    Write-Host "Testing for vDisk on $($AdapterType).$($diskID)" -NoNewline -ForegroundColor Yellow
    if (@($vDisk).count -ne 1)
    {
        ""
        Write-Warning "Source vDisk not found on $($adapterType).$($diskID)"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }
    # write-host "vDisk is $(($vDisk.size)/1024/1024/1024) GiB in size"

    # Logic for testing disk type
    Write-Host "Testing for correct vDisk type of $($diskType)" -NoNewline -ForegroundColor Yellow
    if (!$vdisk.is_cdrom -and $diskType -eq "CDROM"){$vDiskCorrect = $false; $actualType="DISK"}
    if ($vdisk.is_cdrom -and $diskType -eq "DISK"){$vDiskCorrect = $false; $actualType="CDROM"}

    if ($vdisk.is_cdrom -and $diskType -eq "CDROM"){$vDiskCorrect = $true; $actualType="CDROM"}
    if (!$vdisk.is_cdrom -and $diskType -eq "DISK"){$vDiskCorrect = $true; $actualType="DISK"}
    
    if ($vDiskCorrect -eq $false)
    {
        ""
        Write-Warning "Incorrect Disk Type specified, $($adapterType).$($diskID) is $($actualType) not $($diskType)"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }

    # Collects the source vdisk address for the specified vDisk, collecting all vDisks and filtering for correct VM name, adapter and DiskID (the uuid properties of the vDisk objects above are incorrect, unsure why)
    Write-Host "Collecting Source vDisk Address" -NoNewline -ForegroundColor Yellow
    $srcCntnr = Get-XtrdVdisks -prism $prism
    $vdisksOnVM = $srcCntnr | Where-Object {$_.attached_vmname -eq $vmName}
    $vdisksWithIDandType = $vdisksOnVM | Where-Object {$_.disk_address -eq "$adapterType.$diskID"}
    $srcVdiskAddr = $vdisksWithIDandType.nutanix_nfsfile_path
    if (@($srcVdiskAddr).count -ne 1)
    {
        ""
        Write-Warning "Something went wrong, we couldn't find the address for the vDisk - this shouldn't happen at this stage in the script  :/"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }

    # Gets the destination container and checks for one entry
    Write-Host "Testing for Destination Container" -NoNewline -ForegroundColor Yellow
    $dstCntnr = Get-XtrdStorageContainer -prism $prism -storageContainerName $destContainerName
    if (@($dstCntnr).count -ne 1)
    {
        ""
        Write-Warning "Destination Container name doesn't exist, or more than one was found - be more specific"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }

    # Gets the free space of the destination container and test for enough to take the disk image   
    Write-Host "Testing for Destination Container Free Space" -NoNewline -ForegroundColor Yellow
    $dstCntnrFreeMiB = ($dstCntnr.usage_stats."storage.user_unreserved_free_bytes")/1024/1024
    if ($vDisk.disk_size_mib -gt $dstCntnrFreeMiB)
    {
        ""
        Write-Warning "Destination Container doesn't have enough free space"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }

    # Sets the temporary Img Name for the VM
    $imgName = "$($vmName)-tmp"

    # Checks if the temporary image name that will be created below, already exists in the image service   
    Write-Host "Testing for Existing vDisk Images" -NoNewline -ForegroundColor Yellow
    $imgNameTmp = (Get-XtrdImage -prism $prism -imgName $imgName).name
    if ($imgNameTmp)
    {
        ""
        Write-Warning "Image already exists with the name $imgNameTmp, cannot proceed"
        break
    }
    else
    {
        write-host  ": Done" -foregroundColor Gray
    }
    
    ""
    Write-Host "Pre-flight Checks Completed, ready to migrate vDisk!" -ForegroundColor Green

    # Set the correct variable for image-type, based on the detected disk type
    if ($diskType -eq "DISK"){$imageType="DISK_IMAGE"}
    if ($diskType -eq "CDROM"){$imageType="ISO_IMAGE"}
    # Start building the new image, grab the UUID for the task so that we can wait for the task to finish
    $newImageTaskUUID = New-XtrdImage -prism $prism -srcVdiskAddr $srcVdiskAddr -destContainerName $destContainerName -imageName $imgName -imageType $imageType

    # Wait for the task to complete
    Test-XtrdTaskStatus -prism $prism -taskUUID $newImageTaskUUID -wait -for succeeded -intervalSecs 1 -mins 60

    # Check for success, stop if not found
    if (Test-XtrdTaskStatus -prism $prism -taskUUID $newImageTaskUUID -check -for succeeded)
    {
        Write-Host "vDisk cloned to an Image on Destination Container" -ForegroundColor Yellow -NoNewline
        Write-Host ": Done" -ForegroundColor Gray
    }
    else
    {
        Write-Warning "Task did not complete"
        Test-XtrdTaskStatus -prism $prism -taskUUID $newImageTaskUUID -report
        break
    }

    # Delete the original vDisk ($vdisk.disk_address.vmdisk_uuid)
    $removeVdiskTaskUUID = remove-XtrdVdisk -prism $prism -vDiskUUID $vdisk.disk_address.vmdisk_uuid -vmUUID $vm.uuid

    # Wait for the task to complete
    Test-XtrdTaskStatus -prism $prism -taskUUID $removeVdiskTaskUUID -wait -for succeeded -quiet -intervalSecs 2 -mins 1

    # Check for success, stop if not found
    $success = Test-XtrdTaskStatus -prism $prism -taskUUID $removeVdiskTaskUUID -check -for succeeded
    if ($success -eq $false)
    {
        Write-Warning "Task did not complete"
        Test-XtrdTaskStatus -prism $prism -taskUUID $removeVdiskTaskUUID -report
        break
    }
    else
    {
        Write-Host "Original vDisk Detached (Deleted) from VM" -ForegroundColor Yellow -NoNewline
        Write-Host ": Done" -ForegroundColor Gray
    }
    
    # Grab the UUID for the Image vDisk and the Image
    $imagevDiskUUID = (Get-XtrdImage -prism $prism -imgName $($vmName)).vm_disk_id
    $imageUUID = (Get-XtrdImage -prism $prism -imgName $($vmName)).uuid

    # Handle the possibility that it's a CDROM
    if ($imageType -eq "CDROM"){$isCDROM = "true"}
    else {$isCDROM = "false"}

    # Clone the Template disk onto the VM (vDisk Attach) - putting it back onto the same adapter and device ID as before
    $attachVdiskTaskUUID = New-XtrdVdisk -prism $prism -vmUUID $vm.uuid -clone -vdiskImageUUID $imagevDiskUUID -diskAddress $diskID -adapterType $adapterType -isCDROM $isCDROM
    
    # Wait for the task to complete
    Test-XtrdTaskStatus -prism $prism -taskUUID $attachVdiskTaskUUID -wait -for succeeded -quiet -intervalSecs 2 -mins 1

    # Check for success, stop if not found
    $success = Test-XtrdTaskStatus -prism $prism -taskUUID $attachVdiskTaskUUID -check -for succeeded
    if ($success -eq $false)
    {
        Write-Warning "Task did not complete"
        Test-XtrdTaskStatus -prism $prism -taskUUID $attachVdiskTaskUUID -report
        break
    }
    else
    {
        Write-Host "Cloned vDisk Attached to VM" -ForegroundColor Yellow -NoNewline
        Write-Host ": Done" -ForegroundColor Gray
    }
    
    # Trash the temporary image that was created
    $removeImageTaskUUID = Remove-XtrdImage -prism $prism -imageUUID $imageUUID
    
    # Wait for the task to complete
    Test-XtrdTaskStatus -prism $prism -taskUUID $removeImageTaskUUID -wait -for succeeded -quiet -intervalSecs 2 -mins 1

    # Check for success, stop if not found
    $success = Test-XtrdTaskStatus -prism $prism -taskUUID $removeImageTaskUUID -check -for succeeded
    if ($success -eq $false)
    {
        Write-Warning "Task did not complete, vDisk Image Persists!"
        Test-XtrdTaskStatus -prism $prism -taskUUID $removeImageTaskUUID -report
        break
    }
    else
    {
        Write-Host "Removal of temporary vDisk Image" -ForegroundColor Yellow -NoNewline
        Write-Host ": Done" -ForegroundColor Gray
    }

    # Prompt the User to restart the VM
    ""
    Write-Host "Storage Migration Completed, don't forget to restart your VM." -ForegroundColor Green
}

function Get-XtrdVM()
{
    # Returns a global object ($vm) for a given VM Name, containing all key details
    # e.g. use "$vm = Get-XtrdVM -prism 10.21.22.37 -vmName DC1" to create an object to hold the VM details

    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$vmName
    )

    # Grab the UUID for the VM
    $apiVer = "v2"
    $apiPath = "vms/?include_vm_disk_config=true&include_vm_nic_config=true"
    $method = "get"
    $body =
@"
    {
    }
"@
    $getVMResponse = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    $vms = ($getVMResponse | ConvertFrom-Json).entities
    
    # Filter for the right name
    $vm = $vms | Where-Object {$_.name -eq $($vmName)}

    # Return the key VM details
    $vm
}

function New-XtrdVM()
{
    # THIS CMDLET IS WORK IN PROGRESS - the JSON has issues at the moment

    # Support creation with and without supplied image file
    $apiVer = "v2"
    $apiPath = "vms"
    $method = "post"
    $body =
@"
    {
        "description": "Server Template",
        "memory_mb": 4096,
        "name": "Server 2012r2 Base",
        "num_cores_per_vcpu": 2,
        "num_vcpus": 1,
        "storage_container_uuid": "$($StorageContainerUUID)",
        "vm_customization_config": {
        "datasource_type": "CONFIG_DRIVE_V2",
        "fresh_install": false,
        "userdata": "unattend xml in here"
        },
        "vm_disks": [
        {
            "disk_address": {
            "device_bus": "IDE",
            "device_index": 0
            },
            "is_cdrom": true,
            "is_empty": true,
            "is_scsi_pass_through": true,
            "is_thin_provisioned": true,
            "vm_disk_clone": {
            "disk_address": {
                "device_bus": "SCSI",
                "device_index": 1,
                "vmdisk_uuid": "$($vdiskImageUUID)"
            }
            }
        }
        ],
        "vm_nics": [
        {
            "adapter_type": "E1000",
            "network_uuid": "$($NetworkUUID)",
            "request_ip": true
        }
        ]
    }
"@
    Write-Progress -msg "Building new VM Object"
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    write-host "$($response.StatusDescription)" -ForegroundColor Blue -NoNewline
    ($response | ConvertFrom-Json).entities.uuid
}

function Show-XtrdBanner()
{
    $preheader = 
@"
        Presenting:                                
"@
        $header =
@"
               _                     _             
              | |                   | |            
        __  __| |_  _ __  _   _   __| |  ___       
        \ \/ /| __|| '__|| | | | / _`  | / _ \      
         >  < | |_ | |   | |_| || (_| ||  __/      
        /_/\_\ \__||_|    \__,_| \__,_| \___|      
                                                   
"@
        $postHeader = 
@"
        Automating Nutanix Tasks Since 2017        
        ===================================        
"@
    Clear-Host
    write-host $preheader -ForegroundColor Green -BackgroundColor DarkBlue
    write-host $header -ForegroundColor DarkBlue -BackgroundColor Green
    write-host $postheader -ForegroundColor Green -BackgroundColor DarkBlue
}

function Disable-XtrdPulse()
{
    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism
    )

    # Grab the the vDisk template image
    $apiVer = "v1"
    $apiPath = "pulse"
    $method = "put"
    $body =
@"
    {
        "emailContactList":null,
        "enable":false,
        "verbosityType":null,
        "enableDefaultNutanixEmail":false,
        "defaultNutanixEmail":null,
        "nosVersion":null,
        "isPulsePromptNeeded":false,
        "remindLater":null
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    ($response | ConvertFrom-Json).entities
}

function Confirm-XtrdEula()
{
    # Allow advanced parameter functions
    [CmdletBinding()]
    # Define the Parameters to pass the to function
    Param(
    [Parameter(Mandatory=$true)]
    [string]$prism,
    [Parameter(Mandatory=$true)]
    [string]$userName,
    [Parameter(Mandatory=$true)]
    [string]$companyName,
    [Parameter(Mandatory=$true)]
    [string]$jobTitle
    )

    # Grab the the vDisk template image
    $apiVer = "v1"
    $apiPath = "eulas/accept"
    $method = "post"
    $body =
@"
    {
        "username":"$($userName)",
        "companyName":"$($companyName)",
        "jobTitle":"$($jobTitle)"
    }
"@
    $response = Invoke-XtrdPrismAPI -prism $prism -body $body -method $method -apiPath $apiPath -apiVer $apiVer
    ($response | ConvertFrom-Json).entities
}
