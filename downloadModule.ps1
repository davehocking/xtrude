<#  .SYNOPSIS
    version 0.2 - Written by Dave Hocking 2018.

    This script is a dirty hack to download bitbucket projects that are powershell modules
    It doesn't do anything clever with bitbucket, doesn't use Git at all, doesn't check for the need to copy/replace files
    It's inefficient, but, has low requirements and is intended as a stop-gap

    .DESCRIPTION
    This script was originally written for Windows, then adapted for MacOS and PowerShell Core.
    It should be obvious how to amend this script to support Linux too, I just didn't make it a priority.

    MacOS
    =====


    WINDOWS
    =======
    If you run the script without admin rights, you are presented with the option to install into the user's personal modules folder:
    <userprofile>\documents\windowspowershell\modules
    
    If you execute the script with admin rights, the install will automatically proceed into the system's modules folder:
    <windows>\system32\windowspowershell\v1.0\modules

    ...if installed into the system, all users can access the module's functions.

    If verbose output is required, use -v at execution time

    .EXAMPLE
    Installs into either the systemroot or userprofile Module stores, depending on access rights
    
    downloadModule.ps1 

    .EXAMPLE
    Installs into either the systemroot or userprofile Module stores, depending on access rights
    Displays verbose output during execution

    downloadModule.ps1 -v
#>

# Set the Project Name this script is designed to work for
[string]$projName = "xtrude"
[string]$bitbucketAccount = "davehocking"

# Define URL to get the files from
[string]$bitbucket = "https://bitbucket.org"

# Define the read-only account                                                                  
[string]$user = "dave.hocking@nutanix.com"                                                                                               
[string]$pass = "nx2Tech490!"                                                                                                        
[string]$pair = "${user}:${pass}"      

# Encode the credentials                                                                                                
$bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)                                                                         
[string]$base64 = [System.Convert]::ToBase64String($bytes) 

# Build the header                                                                                                
[string]$basicAuthValue = "Basic $base64"
[System.Collections.Hashtable]$headers = @{ Authorization = $basicAuthValue }     

# Detect verbose mode, set variable
if ($args[0] -eq "-v"){[string]$v = $true}
$args[0]

# Function declaration
function done
{
    if ($v)
    {
        Write-Host "Done." -ForegroundColor Green
        ""
    }
}

function move-and-delete
{
    # The archive creates a .git folder, move the contents to the parent directory
    if ($v)
    {
        Write-Host "    Moving files..."
    }
    Move-Item "$($destination)\$($bitbucketAccount)-$($project)-*\*" -Destination "$($destination)\"
    
    # The archive creates a .git folder, this is now useless, trash it
    if ($v)
    {
        Write-Host "    Cleaning up extra .git directory..."
    }
    Remove-Item "$($destination)\$($bitbucketAccount)-$($project)-*" -Recurse

    # The archive contains a tempFiles folder, used for my own testing, this is useless, trash it
    if ($v)
    {
        Write-Host "    Cleaning up extra tempFiles directory..."
    }
    Remove-Item "$($destination)\tempFiles" -Recurse -Confirm:$false
    done
}

function unpack
{
    # Unzip the archive to the destination
    if ($v)
    {
        Write-Host "    Working on " -NoNewline
        Write-Host "$project" -NoNewline -ForegroundColor Yellow
        Write-Host ", unzipping..."
    }

    # Detect OS Type and handle the unzip operation
    if ([environment]::OSVersion.Platform -eq "Win32NT"){Unzip $outfile $destination}
    else {unzip -q $outfile -d $destination}
}

# Detect OS Version and Handle
if ([environment]::OSVersion.Platform -eq "Win32NT")
{
    # Detect if admin rights have been used
    if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator"))
    {
        # No rights detected - install into user profile
        if ($v)
        {
            Write-Host "No Admin rights detected" -ForegroundColor Magenta
        }
        [string]$outpath = "$($env:USERPROFILE)\Documents\WindowsPowerShell\modules"
        [string]$question = read-host "Do you want to install into `"$($outpath)`" [y] or Enter to confirm"
        switch ($question)
        {
            y       {}
            ""      {}
            default {Write-Host "Stopping."; break}
        }
    }        
    else
    {
        if ($v)
        {
            Write-Host "Admin rights detected" -ForegroundColor Green
        }
        # Admin rights used - install into system profile
        [string]$outpath = "$($env:SystemRoot)\System32\WindowsPowerShell\v1.0\Modules"
    }
}

if ([environment]::OSVersion.Platform -eq "Unix")
{
    # Detect if admin rights have been used
    if ($(id -u) -ne 0)
    {
        # No rights detected - install into user profile
        if ($v)
        {
            Write-Host "No Superuser rights detected" -ForegroundColor Magenta
        }
        [string]$outpath = "~/.local/share/powershell/Modules/"
        [string]$question = read-host "Do you want to install into `"$($outpath)`" [y] to confirm"
        if ($question -match "y"){}
        else {Write-Host "Stopping."; break}
    }        
    else
    {
        if ($v)
        {
            Write-Host "Superuser rights detected" -ForegroundColor Green
        }
        # Admin rights used - install into system profile
        [string]$outpath = "$($PSHOME)/Modules"
    }
}

# Windows needs an "Unzip" function, MacOS can use it's native tools
if ([environment]::OSVersion.Platform -eq "Win32NT")
{
    # Set up script to be able to unzip compressed archives (PowerShell v4 lacked this feature)
    if ($v)
    {
        Write-Host "Setting up ZIP de/compression functionality..."
    }

    # Create unzip function
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    function Unzip
    {
        param([string]$zipfile, [string]$outpath)
        [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
    }
    done
}

# Create the Projects Array, for holding the names of the projects on bitbucket
if ($v)
{
    Write-Host "Creating Projects Array..."
}
[array]$projects = @()
$projects += $($projName)
done

# Loop through the array and download the latest build from the bitbucket Server
if ($v)
{
    Write-Host "Looping through the projects, downloading and unpacking..."
}
foreach ($project in $projects)
{
    # Set the URLs/Paths to be used and download the Archive file
    $url = "$($bitbucket)/$bitbucketAccount/$($project)/get/master.zip"

    # Detect OS Type and use the native temp directory
    if ([environment]::OSVersion.Platform -eq "Win32NT")
    {
        $outfile = "c:\windows\temp\$($project).zip"
        $destination = "$($outpath)\$($project)"
    }
    else
    {
        $outfile = "/tmp/$($project).zip"
        $destination = "$($outpath)/$($project)"
    }

    Invoke-WebRequest -Uri $url -OutFile $outfile -ErrorAction Stop -ErrorVariable fatal -Headers $headers
    if ($fatal)
    {
        Write-Warning "Failed to download file - stopping process to prevent accidental removal of old versions"
        break
    }

    # Check if the folder already exists (if it does -> trash it, if not, then just extract)
    if (!(Test-Path $destination))
    {
        unpack
        move-and-delete
    }
    else
    {
        # As the destination already exists, trash it, you'll be dropping a fresh copy off
        if ($v)
        {
            Write-Host "    Project already exists, trashing local copy..."
        }
        Remove-Item $destination -Confirm:$false -Recurse
        unpack
        move-and-delete
    }
}