<#
    THIS CMDLET IS USED TO CONDUCT THE BUILD PROCESS FOR HPOCs
    For example...

    buildHPOC.ps1

    Then supply something like the following info

-prism 10.21.22.37 -vmNetAddr 10.21.22.0 -vmNetCIDRbits 25 -vmNetGWaddr 10.21.22.1
#>

# Allow advanced parameter functions
[CmdletBinding()]
# Define the Parameters to pass the to function
Param(
[Parameter(Mandatory=$true)]
[string]$prism,
[Parameter(Mandatory=$false)]
[string]$vmNetName="Primary-VMs",
[Parameter(Mandatory=$false)]
[string]$vmNetVLAN=0,
[Parameter(Mandatory=$true)]
[string]$vmNetAddr,
[Parameter(Mandatory=$true)]
[string]$vmNetCIDRbits,
[Parameter(Mandatory=$true)]
[string]$vmNetGWaddr,
# Use the network address specified to make a guess of the pool size, works for empty /25 and larger nets
[Parameter(Mandatory=$false)]
[string]$vmNetIPstart=$vmNetAddr.Split(".")[0]+"."+$vmNetAddr.Split(".")[1]+"."+$vmNetAddr.Split(".")[2]+"."+"50",
[Parameter(Mandatory=$false)]
[string]$vmNetIPstop=$vmNetAddr.Split(".")[0]+"."+$vmNetAddr.Split(".")[1]+"."+$vmNetAddr.Split(".")[2]+"."+"119",
[Parameter(Mandatory=$false)]
# Make an assumption for the IP/Domain Name of the first Domain Controller you'll be building
[string]$vmNetDNS=$vmNetAddr.Split(".")[0]+"."+$vmNetAddr.Split(".")[1]+"."+$vmNetAddr.Split(".")[2]+"."+"101",
[Parameter(Mandatory=$false)]
[string]$vmNetDomain="ntnxlab.local",
# What's the full name of the vDisk image that will be used for cloning?
[Parameter(Mandatory=$false)]
[string]$vdiskImageName="Windows 2012"
)

# Start a stage counter, for outputting progress later
$global:stage = 0


function Write-Progress()
{
        # Allow advanced parameter functions
        [CmdletBinding()]

        # Define the Parameters to pass the to function
        Param(
        [Parameter(Mandatory=$true)]
        [string]$msg,
        [Parameter(Mandatory=$false)]
        [switch]$increment
        )
    if ($PSBoundParameters.ContainsKey('increment')) 
    {
        $global:stage = $stage+1
        Write-Host ""
        Write-Host "Stage $($stage.ToString("00"))" -NoNewline -ForegroundColor Yellow
    }  
    else
    {
        write-Host "        " -NoNewline 
    }

    Write-Host " : " -ForegroundColor DarkYellow -NoNewline
    Write-Host $msg  
}

# Display header banner
Show-XtrdBanner

<#
    Step 1 - Establish VPN
#>
Write-Progress -msg "Establish VPN or direct comms with the required Nutanix Cluster" -increment
#pause

# Test for Prism reachability on 9440
Write-Progress -msg "Testing Prism Element Reachability"
$test = Test-Connection $prism -Quiet -TCPPort 9440
if ($test -eq $false)
{
    Write-Warning "Cannot connect to prism on 9440"
    break
}
else
{
    Write-Host "Info     : Responding on TCP:9440 " -NoNewline
    Write-Host "$test" -ForegroundColor Blue
}

<#
    Step 2 - EULA/PulseHD
    There doesn't seem to be a published API for these tasks, one to track down later
    02a) Accept EULA
#>
Write-Progress -msg "Accepting the EULA, Disabling Pulse" -increment
Confirm-XtrdEula -prism $prism -userName "xtrude HPOC Script" -companyName "Nutanix" -jobTitle "DEMO-ONLY"
Disable-XtrdPulse -prism $prism

<#
    02c) Check for AHV Hypervisor (Required)
#>

Write-Progress -msg "Checking for correct Hypervisor"
$ntnxClustertype = Get-XtrdClusterType -prism $prism
if ($ntnxClustertype -ne "AHV")
{
    Write-Warning "The target cluster isn't using AHV, other hypervisors haven't been coded for as yet"
    break
}
else
{
    Write-Host "Info     : We found " -NoNewline
    Write-Host "$($ntnxClusterType)" -ForegroundColor Blue
}

<#
    Step 3 - Create Storage container "VM-Data", in default storage pool (v2 API)
    03a) 4000GiB Advertised, Compression w/ 0m delay
#>

# Check for the presence of the Storage Container first
$storageContainerName = "VM-Data"
Write-Progress -msg "Checking for Storage Container as `"$($storageContainerName)`"" -increment
$storageContainerUUID = ((Get-XtrdStorageContainer -prism $prism -storageContainerName $storageContainerName) | Where-Object {$_.name -eq $storageContainerName}).storage_container_uuid

if (!$storageContainerUUID)
{
    $storageContainerSizeGiB = 4000
    Write-Progress -msg "Creating $($storageContainerSizeGiB)GiB, Inline-Compressed Storage Container"
    $containerTask = New-XtrdStorageContainer -prism $prism -containerName $storageContainerName -compressDelayMins 0 -sizeGiB $storageContainerSizeGiB

    # Check if the task completed properly or not
    if(!$containerTask)
    {
        Write-Warning "Couldn't build the Container"
        break
    }

    # Get the storage container UUID for the container you just built, or output the UUID for the one that already exists
    $StorageContainerUUID = (Get-XtrdStorageContainer -prism $prism -storageContainerName $storageContainerName).storage_container_uuid
    Write-Host "         : UUID is " -NoNewline
    Write-Host $($StorageContainerUUID) -ForegroundColor Cyan
}
else
{
    Write-Host "Info     : Container with the name `"$($storageContainerName)`" already exists"
    Write-Host "Info     : UUID is " -NoNewline
    Write-Host $($StorageContainerUUID) -ForegroundColor Cyan
}

<#
    Step 4 - Network Configuration > User VM Interfaces
    04a) Check for existing networks with the same name
    04b) Check for existing networks using the same vlan_id
    04c) Create "Primary VMs" Network, vlan-id 0
            Enable IPAM, specify network + CIDR, gateway IP
            Specify DNS server as 10.21.32.101, Domain Search & Name both as "ntnxlab.local"
            Create IP Pool, starting at .80, ending at .119
#>
# Check for the presence of the Network Name first
Write-Progress -msg "Checking for Network Name as `"$($vmNetName)`"" -increment
$networkUUID = (Get-XtrdNetwork -prism $prism -networkName $vmNetName).uuid
if (!$networkUUID)
{
    # Now check for the presence of the same VLAN ID in another network
    #   Write-Progress -msg "Checking for Network VLAN id : `"$($vmNetVLAN)`""
    #   $networkUUID = (Get-XtrdNetwork -prism $prism -networkVLANid $vmNetVLAN).uuid

    # And if none are returned, go ahead and create one
    #   if (!$networkUUID)
    #   {
        Write-Progress -msg "Creating `"$($vmNetName)`" User Network, on VLAN:$($vmNetVLAN) with IPAM"
        $netTaskUUID = New-XtrdNetwork -prism $prism -name $vmNetName -vlanID 0 -vmNetaddr $vmNetAddr -vmNetCIDRbits $vmNetCIDRbits -vmNetGWaddr $vmNetGWaddr -vmNetIPstart $vmNetIPstart -vmNetIPstop $vmNetIPstop -vmNetDomain $vmNetDomain -vmNetDNS $vmNetDNS

        # Check if the task completed properly or not
        if(!$netTaskUUID)
        {
            Write-Warning "Couldn't build the Network"
            break
        }

        # Grab the returned UUID for the network, it will be used later
        $networkUUID = $netTaskUUID
        Write-Host "         : UUID is " -NoNewline
        Write-Host $($networkUUID) -ForegroundColor Cyan  
    #   }
    #   else
    #   {
    #       Write-Host "Info: Network with the VLAN `"$($vmNetVLAN)`" already exists"
    #       Write-Host "UUID is " -NoNewline
    #       Write-Host $($NetworkUUID) -ForegroundColor Cyan
    #       Write-Warning "You need to choose a VLANid not already assigned"
    #       Break    
    #   }

}
else
{
    Write-Host "Info     : Network with the name `"$($vmNetName)`" already exists"
    Write-Host "Info     : UUID is " -NoNewline
    Write-Host $($NetworkUUID) -ForegroundColor Cyan    
}
#pause

<#
    Step 5 - UI Tweaks
    There doesn't seem to be a published API for these tasks, one to track down later
    05a) UI Settings, Disable Animation
    05b) Current User Timeout to 30mins
#>
Write-Progress -msg "Settings > UI Settings > Disable Background Animation" -increment
Write-Progress -msg "Settings > UI Settings > Increase Current User Timeout"
pause

<#
    Step 6 - Windows VM Template Build
    06a) Get the UUID of the template vDisk
#>
Write-Progress -msg "Getting UUID of Template vDisk from Image Store" -increment
$vdiskImageUUID = (Get-XtrdImage -prism $prism -imgName $vdiskImageName).vm_disk_id
# Check that a UUID was returned
if ($vdiskImageUUID)
{
    Write-Host "         : UUID is " -NoNewline
    Write-Host $($vdiskImageUUID) -ForegroundColor Cyan    
}
else
{
    Write-Host "         : " -NoNewline
    Write-Warning "No UUID found matching the filter"
    break
}
# If none exists, one can be downloaded

<#
    06b) Create VM "Server 2012r2 Base", description "Template VM"
        1x vCPU, 2 Cores, 4GiB RAM
        Add new Disk, Clone from Image Service, SCSI 
        Add new NIC, "Primary VMs" Network, No IP
#>
# Build a new VM entity using the UUID of the template vDisk

<#
    06b) Power-ON VM
#>
